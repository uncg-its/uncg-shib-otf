<?php
use Composer\Script\Event;

/**
 * Installer to be used when composer is installing the base app.
 */
class Uncgshibcomposer
{
    public static function setupSymlinks(Event $event)
    {
        $paths = self::_helperDirectory($event);

        $io = $event->getIO();
        
        if (!file_exists($paths['basePath'] . '/library/Internal/Auth/Adapter')) {
            mkdir($paths['basePath'] . '/library/Internal/Auth/Adapter', 0777, true);
        }
        
        $foldersToLink = array(
            '/library/Internal/Auth/Adapter/Uncg',
        );

        foreach ($foldersToLink as $f) {
            @unlink($paths['basePath'] . $f);
            symlink($paths['uncgAuthAdapterPath'], $paths['basePath'] . $f);
            $io->write('Symlinking ' . $f);
        }
        
        // now symlink the shib directory that needs to go into the root
        @unlink($paths['basePath'] . '/shib');
        symlink($paths['uncgShibDirectoryPath'], $paths['basePath'] . '/shib');
        $io->write('Symlinking ' . '/shib');

    }

    public static function _helperDirectory(Event $event)
    {
        return array(
            'basePath'                  => realpath($event->getComposer()->getConfig()->get('vendor-dir') . '/../'),
            'uncgAuthAdapterPath'       => realpath($event->getComposer()->getConfig()->get('vendor-dir') . '/uncgits/uncg-shib-otf/uncg'),
            'uncgShibDirectoryPath'       => realpath($event->getComposer()->getConfig()->get('vendor-dir') . '/uncgits/uncg-shib-otf/shib'),
        );
    }

}
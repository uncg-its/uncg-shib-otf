<?php

class Internal_Auth_Adapter_Uncg_Uncgshiball implements Zend_Auth_Adapter_Interface, Ot_Auth_Adapter_Interface
{

    /**
     * Username of the user to authenticate
     *
     * @var string
     */
    protected $_username = '';

    /**
     * Password of the user to authenticate
     *
     * @var string
     */
    protected $_password = '';

    /**
     * Constant for default username for auto-login
     *
     */
    const defaultUsername = '';

    /**
     * Constant for default password for auto-login
     *
     */
    const defaultPassword = '';
    
    /**
     * Constructor to create new object
     *
     * @param string $username
     * @param string $password
     */
    public function __construct($username = self::defaultUsername, $password = self::defaultPassword)
    {
        $this->_username = $username;
        $this->_password = $password;
    }

    /**
     * Authenticates the user passed by the constructor, however in this case we
     * user the WRAP server variable "WRAP_USERID" to get this appropriate username.
     *
     * @return new Zend_Auth_Result object
     */
    public function authenticate()
    {
        
        $username = strtolower(substr($_SERVER["eppn"],0,strpos($_SERVER["eppn"], '@')));
        $affiliation = strtolower(substr($_SERVER["affiliation"],0,strpos($_SERVER["affiliation"], '@')));
        $emailAddress = strtolower($_SERVER["eppn"]);
        
        if ($username == '') {
        
            $applicationBaseUrl = preg_replace('/\/login$/', '', $this->_getUrl());

            setrawcookie("SHIB_REFERER", $this->_getUrl(), 0, '/', $applicationBaseUrl);
        
            header('location:' . $applicationBaseUrl . '/shib');
            
            die();
        }
        
        if (strtolower($username) == 'guest') {
            $this->autoLogout();
            return new Zend_Auth_Result(
               false,
               new stdClass(),
               array('Guest access is not allowed for this application')
            );
        }
        
        //if (1){ used for testing if student if we don't have a student only account
        if ($affiliation != 'faculty' && $affiliation != 'staff' && $affiliation != 'student') {
            $this->autoLogout();
            $class = new stdClass();
            $class->authenticatedStudent = true;
            return new Zend_Auth_Result(
               false,
               $class,
               array('This application is available for UNCG Faculty, Staff, and Students Only.')
            );
        }

        $class = new stdClass();
        $class->username = $username;
        $class->firstName = $_SERVER["givenName"];
        $class->lastName = $_SERVER["sn"];
        $class->emailAddress = $emailAddress;
        $class->realm    = 'uncgshib';
        
        return new Zend_Auth_Result(true, $class, array());
    }

    /**
     * Gets the current URL
     *
     * @return string
     */
    protected function _getURL()
    {
        $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";

        $protocol = substr(
            strtolower($_SERVER["SERVER_PROTOCOL"]), 0, strpos(strtolower($_SERVER["SERVER_PROTOCOL"]), "/")
        ) . $s;

        $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);

        return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI'];
    }

    /**
     * Setup this adapter to autoLogin
     *
     * @return boolean
     */
    public static function autoLogin()
    {
        return true;
    }

    /**
     * Logs the user out by removing all the Shib cookies that are created.
     *
     */
    public static function autoLogout()
    {
        
        $applicationBaseUrl = preg_replace('/\/login\/logout$/', '', $this->_getUrl());
        
        foreach (array_keys($_COOKIE) as $name) {
            if (preg_match('/^SHIB_.*/', $name)) {

                // Set the expiration date to one hour ago
                setcookie($name, "", time() - 3600, "/", $applicationBaseUrl);
            }
        }
    }

    /**
     * Flag to tell the app where the authenticaiton is managed
     *
     * @return boolean
     */
    public static function manageLocally()
    {
        return false;
    }
    
    /**
     * flag to tell the app whether a user can sign up or not
     *
     * @return boolean
     */
    public static function allowUserSignUp()
    {
        return false;
    }

}